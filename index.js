let http = require("http");

http.createServer(function(req, res){

	if (req.url == "/" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to Booking System");
	}
	if (req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Welcome to your profile!");
	}

	if (req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Here's our courses available.");
	}
	if (req.url == "/addCourse" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Add course to our resources.");
	}

	if (req.url == "/addCourse" && req.method == "POST"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Add course to our resources.");
	}
	if (req.url == "/updateCourse" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update a course to our resouces.");
	}
	if (req.url == "/updateCourse" && req.method == "PUT"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Update a course to our resouces.");
	}
	if (req.url == "/archiveCourse" && req.method == "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Archive courses to our resources.");
	}
	if (req.url == "/archiveCourse" && req.method == "DELETE"){
		res.writeHead(200, {"Content-Type": "text/plain"});
		res.end("Archive courses to our resources.");
	}

}).listen(4000);

console.log("System is now running at localhost 4000!");

